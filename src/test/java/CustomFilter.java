import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;

public class CustomFilter implements Filter {
	@Override
	public Response filter(FilterableRequestSpecification filterableRequestSpecification, FilterableResponseSpecification filterableResponseSpecification, FilterContext filterContext) {
		filterableRequestSpecification.baseUri(filterableRequestSpecification.getBaseUri() + "test/");
		return filterContext.next(filterableRequestSpecification, filterableResponseSpecification);
	}

}
