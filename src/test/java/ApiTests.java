import io.restassured.RestAssured;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.lessThan;

public class ApiTests {

	RequestSpecification  requestSpecBuilder;
	ResponseSpecification responseSpecification;

	public static Map<Object, Object> map = new HashMap<Object, Object>();

	@BeforeMethod
	public void init() {
		CustomFilter customFilter = new CustomFilter();

		requestSpecification = given().baseUri("http://wiremock.atlantis.t-systems.ru/")
									  .filters(new ResponseLoggingFilter(), new ResponseLoggingFilter(), new CustomFilter());

		//		Map<String, String> map1 = new HashMap<String, String>();
		//		map1.put("method", "GET");
		//		map1.put("url", "/test/aburmatnova");
		//		map.put("request", map1);
		//		Map<String, String> map2 = new HashMap<String, String>();
		//		map2.put("status", "202");
		//		map.put("response", map2);

		responseSpecification = expect()
				.statusCode(200)
				.contentType("application/json"); //was changed because of mapping updateTest

	}

	@Test
	public void mappingTest() {
		given().spec(requestSpecification)
			   .expect()
			   .spec(responseSpecification);
		RestAssured.when()
				   .get("aburmatnova")
				   .then()
				   .time(lessThan(5000L)) // endpoint was changed. Because of custom filter (Adds /test/)

				   .statusCode(200)
				   .body("$", Matchers.hasEntry("name", "Anna"))
				   .body("$", Matchers.hasEntry("lastname", "Burmatnova"))
				   .body("$", Matchers.hasEntry("test", "test"));

	}
	//
	//	@Test
	//	public void updateMappingTest() {
	//		given().spec(requestSpecBuilder)
	//			   .body(map)
	//			   .when()
	//			   .request("PUT", "__admin/mappings/f62f83fa-49b6-42e3-9699-bdbc91422db0")
	//
	//			   .then()
	//			   .statusCode(200);
	//
	//	}
	//
	//	@Test
	//	public void mappingTestSample() {
	//		given().spec(requestSpecBuilder)
	//			   .when()
	//			   .request("GET", "/tau/test")
	//
	//			   .then()
	//			   .statusCode(202)
	//			   .body("$", Matchers.hasEntry("searchResults", "42"))
	//			   .header("Content-Type", "application/json");
	//
	//	}

}
