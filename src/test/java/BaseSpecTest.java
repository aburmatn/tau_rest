import io.restassured.RestAssured;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.lessThan;

public class BaseSpecTest {

	@BeforeMethod
	public void init() {
		RestAssured.baseURI = "http://wiremock.atlantis.t-systems.ru/";
		RestAssured.filters(new ResponseLoggingFilter(), new ResponseLoggingFilter());

	}

	@Test
	public void mappingTest() {
		RestAssured.when()
				   .get("aburmatnova")
				   .then()
				   .statusCode(200)
				   .body("$", Matchers.hasEntry("name", "Anna"))
				   .body("$", Matchers.hasEntry("lastname", "Burmatnova"))
				   .body("$", Matchers.hasEntry("test", "test"));

	}

	@Test
	public void measureResponseTimeTest() {
		Response response = RestAssured.when()
									   .request("GET", "/test/aburmatnova"); //2 разные переменные для сравнения assert потом
		long timeInSeconds = response.getTimeIn(TimeUnit.SECONDS);
		long timeInMlscs = response.time();
		RestAssured.when()
				   .get("/test/aburmatnova")
				   .then()
				   .time(lessThan(5000L)); // сразу измеряется время после отправки респонса.  Сравнение со хначением в скобках
	}

}